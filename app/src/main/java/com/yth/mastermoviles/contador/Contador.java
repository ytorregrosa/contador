package com.yth.mastermoviles.contador;

import android.app.Service;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.IBinder;
import android.support.annotation.IntDef;
import android.support.annotation.Nullable;
import android.util.Log;
import android.widget.Toast;

/**
 * Created by mastermoviles on 17/11/17.
 */

public class Contador extends Service {

    Integer contador = 1;
    ContadorThread contadorThread;

    @Override
    public void onCreate() {
        contadorThread = new ContadorThread();
        super.onCreate();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if(contadorThread.getStatus() != AsyncTask.Status.RUNNING) {
            contadorThread = new ContadorThread();
            contadorThread.execute();
        }
        return Service.START_STICKY;
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if(contadorThread != null)
            contadorThread.cancel(true);
    }

    private class ContadorThread extends AsyncTask<Void, Integer, Void> {

        @Override
        protected Void doInBackground(Void... params) {
            for(contador = 1; contador < 11; contador++) {
                publishProgress(contador);
                try {
                    Thread.sleep(5000);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onProgressUpdate(Integer... contador) {
            Toast.makeText(getApplicationContext(), "Contador: "+contador[0], Toast.LENGTH_SHORT)
                    .show();
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            stopSelf();
        }

        @Override
        protected void onCancelled() {
            stopSelf();
        }
    }



}
