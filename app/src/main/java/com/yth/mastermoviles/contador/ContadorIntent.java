package com.yth.mastermoviles.contador;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.util.Log;
import android.widget.Toast;

/**
 * Created by mastermoviles on 17/11/17.
 */

public class ContadorIntent extends IntentService {

    int contador;
    Handler handler;

    public ContadorIntent() {
        super("ContadorIntent");
        handler = new Handler();
        contador = 1;
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        while (contador >= 1 && contador < 11) {
            handler.post(new ShowToast(this, "Contador intent: "+ contador));
            contador++;
            try {
                Thread.sleep(5000);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        stopSelf();
        contador = 0;
    }

    public class ShowToast implements Runnable {
        private final Context context;
        String message;

        public ShowToast(Context ctx, String msg){
            this.context = ctx;
            this.message = msg;
        }

        public void run(){
            Toast.makeText(this.context, this.message, Toast.LENGTH_SHORT).show();
        }
    }


}
