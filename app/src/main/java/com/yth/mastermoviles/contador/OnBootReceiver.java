package com.yth.mastermoviles.contador;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

/**
 * Created by mastermoviles on 17/11/17.
 */

public class OnBootReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        Intent servicio = new Intent(context, ContadorIntent.class);
        context.startService(servicio);
    }
}
